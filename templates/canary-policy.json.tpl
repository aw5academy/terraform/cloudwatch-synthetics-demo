{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:${aws_account_id}:log-group:cwsyn-cw-synth-demo-*:log-stream:*",
        "arn:aws:s3:::cw-syn-results-*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:CreateLogGroup",
        "s3:GetBucketLocation"
      ],
      "Resource": [
        "arn:aws:logs:*:${aws_account_id}:log-group:cwsyn-cw-synth-demo-*",
        "arn:aws:s3:::cw-syn-results-*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData",
        "s3:ListAllMyBuckets"
      ],
      "Resource": "*"
    },
    {
      "Effect":"Allow",
      "Resource": "*",
      "Action":[
        "ec2:CreateNetworkInterface",
        "ec2:DescribeDhcpOptions",
        "ec2:DescribeNetworkInterfaces",
        "ec2:DeleteNetworkInterface",
        "ec2:DescribeSubnets",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs"
      ]
    }
  ]
}
