variable "app" {
  default     = "cloudwatch-synthetics-demo"
  description = "The string to use for the App tag that will be applied to all created resources."
}

variable "canary_name" {
  default     = "myapp"
  description = "The name of the CloudWatch Synthetics Canary that will be created."
}

variable "container-port" {
  description = "The container port that will accept requests from the ALB."
  default = "80"
}

variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}

variable "test-listener-port" {
  default     = "8080"
  description = "The port for the test listener on the ALB used in the blue/green deployments."
}
