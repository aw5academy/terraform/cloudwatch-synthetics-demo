resource "aws_ecs_cluster" "app" {
  configuration {
    execute_command_configuration {
      kms_key_id = aws_kms_key.session-manager.arn
      logging    = "OVERRIDE"

      log_configuration {
        s3_bucket_encryption_enabled = "true"
        s3_bucket_name = aws_s3_bucket.ecs-exec-logs.id
      }
    }
  }
  name = var.app
  tags = {
    Name = var.app
  }
}

# Define a placeholder task definition just so we can launch the ECS service with Terraform.
# Our CodeDeploy deployments will create the actual task definition.
resource "aws_ecs_task_definition" "app" {
  family                   = var.app
  container_definitions    = templatefile("${path.module}/templates/container_definitions.json.tpl", {})
  cpu                      = "256"
  execution_role_arn       = aws_iam_role.ecs-task-exec-role.arn
  task_role_arn            = aws_iam_role.ecs-tasks.arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "512"
  tags = {
    Name = var.app
  }
}

resource "aws_ecs_service" "app" {
  name                              = var.app
  cluster                           = aws_ecs_cluster.app.id
  task_definition                   = aws_ecs_task_definition.app.arn
  desired_count                     = "1"
  depends_on                        = [aws_lb.app]
  enable_ecs_managed_tags           = "true"
  enable_execute_command            = "true"
  health_check_grace_period_seconds = "60"
  launch_type                       = "FARGATE"
  propagate_tags                    = "SERVICE"
  platform_version                  = "1.4.0"
  deployment_controller {
    type = "CODE_DEPLOY"
  }
  lifecycle {
    ignore_changes = [
      task_definition,
      load_balancer
    ]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.blue.arn
    container_name   = "main"
    container_port   = var.container-port
  }
  network_configuration {
    security_groups  = [aws_security_group.ecs.id]
    subnets          = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
  }
  tags = {
    Name = var.app
  }
}
