{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssmmessages:CreateControlChannel",
        "ssmmessages:CreateDataChannel",
        "ssmmessages:OpenControlChannel",
        "ssmmessages:OpenDataChannel"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": "kms:Decrypt",
      "Resource": "arn:aws:kms:*:${aws_account_id}:key/*",
      "Condition": {
        "ForAnyValue:StringEquals": {
          "kms:ResourceAliases": "${kms-alias}"
        }
      }
    },
    {
      "Effect":"Allow",
      "Resource":[
        "${ecs-exec-logs-bucket-arn}",
        "${ecs-exec-logs-bucket-arn}/*"
      ],
      "Action":[
        "s3:PutObject",
        "s3:GetObject",
        "s3:GetBucketVersioning",
        "s3:GetEncryptionConfiguration"
      ]
    }
  ]
}
