resource "aws_codebuild_project" "app" {
  artifacts {
    type = "CODEPIPELINE"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    environment_variable {
      name = "AWS_ACCOUNT_ID"
      value = data.aws_caller_identity.current.account_id
    }
    environment_variable {
      name = "AWS_REGISTRY_HOST"
      value = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.region}.amazonaws.com"
    }
    environment_variable {
      name = "CONTAINER_PORT"
      value = var.container-port
    }
    environment_variable {
      name = "DOCKER_IMAGE_NAME"
      value = aws_codecommit_repository.app.repository_name
    }
    environment_variable {
      name = "FAMILY"
      value = var.app
    }
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:2.0"
    privileged_mode             = "true"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"
  }
  depends_on = [
    aws_iam_role_policy.codebuild
  ]
  logs_config {
    cloudwatch_logs {
      group_name = aws_cloudwatch_log_group.codebuild.name
    }
  }
  name          = var.app
  service_role  = aws_iam_role.codebuild.arn
  source {
    type = "CODEPIPELINE"
  }
  tags = {
    Name = var.app
  }
  vpc_config {
    vpc_id             = module.vpc.vpc-id
    subnets            = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
    security_group_ids = [aws_security_group.codebuild.id]
  }
}
