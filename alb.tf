resource "aws_lb" "app" {
  internal           = false
  load_balancer_type = "application"
  name               = var.app
  security_groups    = [aws_security_group.lb.id]
  subnets            = [module.vpc.public-a-subnet-id, module.vpc.public-b-subnet-id]
  tags = {
    Name = var.app
  }
}

resource "aws_lb_listener" "live" {
  default_action {
    target_group_arn = aws_lb_target_group.blue.arn
    type             = "forward"
  }
  lifecycle {
    ignore_changes = [
      default_action
    ]
  }
  load_balancer_arn = aws_lb.app.arn
  port              = "80"
}

resource "aws_lb_listener" "test" {
  default_action {
    target_group_arn = aws_lb_target_group.blue.arn
    type             = "forward"
  }
  lifecycle {
    ignore_changes = [
      default_action
    ]
  }
  load_balancer_arn = aws_lb.app.arn
  port              = var.test-listener-port
}

resource "aws_lb_target_group" "blue" {
  name                 = "${var.app}-blue"
  port                 = var.container-port
  protocol             = "HTTP"
  vpc_id               = module.vpc.vpc-id
  target_type          = "ip"
  health_check {
    healthy_threshold   = "2"
    interval            = "30"
    matcher             = "200"
    path                = "/"
    port                = var.container-port
    protocol            = "HTTP"
    timeout             = "5"
    unhealthy_threshold = "2"
  }
  tags = {
    Name = "${var.app}-blue"
  }
}

resource "aws_lb_target_group" "green" {
  name                 = "${var.app}-green"
  port                 = var.container-port
  protocol             = "HTTP"
  vpc_id               = module.vpc.vpc-id
  target_type          = "ip"
  health_check {
    healthy_threshold   = "2"
    interval            = "30"
    matcher             = "200"
    path                = "/"
    port                = var.container-port
    protocol            = "HTTP"
    timeout             = "5"
    unhealthy_threshold = "2"
  }
  tags = {
    Name = "${var.app}-green"
  }
}
