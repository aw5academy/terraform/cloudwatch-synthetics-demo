resource "random_string" "random" {
  length  = 12
  special = false
  upper   = false
  number  = false
}

resource "aws_s3_bucket" "codepipeline-artifacts" {
  acl           = "private"
  bucket        = "codepipeline-artifacts-${random_string.random.result}"
  force_destroy = true
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 30
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name = "codepipeline-artifacts"
  }
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket" "ecs-exec-logs" {
  acl           = "private"
  bucket        = "ecs-exec-logs-${random_string.random.result}"
  force_destroy = true
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 30
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = {
    Name = "ecs-exec-logs"
  }
  versioning {
    enabled = true
  }
}
