{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:${aws_account_id}:log-group:/aws/lambda/${function-name}",
        "arn:aws:logs:*:${aws_account_id}:log-group:/aws/lambda/${function-name}:log-stream:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "synthetics:GetCanaryRuns",
        "synthetics:GetCanary",
        "synthetics:StartCanary"
      ],
      "Resource": "arn:aws:synthetics:*:${aws_account_id}:canary:${canary_name}"
    },
    {
      "Effect": "Allow",
      "Action": [
        "codedeploy:PutLifecycleEventHookExecutionStatus"
      ],
      "Resource": "*"
    }
  ]
}
