[
  {
    "name": "main",
    "image": "httpd:latest",
    "essential": true,
    "linuxParameters": {
      "initProcessEnabled": true
    },
    "portMappings": [
      {
        "hostPort": 80,
        "protocol": "tcp",
        "containerPort": 80
      }
    ]
  }
]
