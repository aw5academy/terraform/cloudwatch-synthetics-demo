resource "aws_cloudwatch_log_group" "ecs" {
  name              = "/aws/ecs/${var.app}"
  retention_in_days = "180"
  tags = {
    Name = "/aws/ecs/${var.app}"
  }
}

resource "aws_cloudwatch_log_group" "codebuild" {
  name              = "/aws/codebuild/${var.app}"
  retention_in_days = "180"
  tags = {
    Name = "/aws/codebuild/${var.app}"
  }
}

resource "aws_cloudwatch_log_group" "deploy-hook" {
  name              = "/aws/lambda/${var.app}-deploy-hook"
  retention_in_days = "180"
  tags = {
    Name = "/aws/lambda/${var.app}-deploy-hook"
  }
}
