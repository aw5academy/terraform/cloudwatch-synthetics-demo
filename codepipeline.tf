resource "aws_codepipeline" "app" {
  name     = var.app
  role_arn = aws_iam_role.codepipeline.arn

  artifact_store {
    location = aws_s3_bucket.codepipeline-artifacts.id
    type     = "S3"
    encryption_key {
      id   = data.aws_kms_key.s3.arn
      type = "KMS"
    }
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      version          = "1"
      output_artifacts = ["SourceArti"]
      configuration = {
        RepositoryName = aws_codecommit_repository.app.repository_name
        BranchName     = "master"
      }
    }
  }
  stage {
    name = "Build"
    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      input_artifacts  = ["SourceArti"]
      output_artifacts = ["BuildArti"]
      version          = "1"
      configuration = {
        ProjectName = aws_codebuild_project.app.name
      }
    }
  }
  stage {
    name = "Deploy"
    action {
      name             = "Deploy"
      category         = "Deploy"
      owner            = "AWS"
      provider         = "CodeDeployToECS"
      input_artifacts  = ["BuildArti"]
      version          = "1"
      configuration = {
        AppSpecTemplateArtifact        = "BuildArti"
        AppSpecTemplatePath            = "appspec.yaml"
        ApplicationName                = aws_codedeploy_app.app.name
        DeploymentGroupName            = aws_codedeploy_deployment_group.app.deployment_group_name
        TaskDefinitionTemplateArtifact = "BuildArti"
        TaskDefinitionTemplatePath     = "taskdef.json"
      }
    }
  }
  tags = {
    Name = var.app
  }
}
