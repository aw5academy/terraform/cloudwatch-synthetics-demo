resource "aws_ecr_repository" "app" {
  name = var.app
  tags = {
    Name = var.app
  }
}

resource "aws_ecr_lifecycle_policy" "app" {
  policy     = templatefile("${path.module}/templates/ecr-lifecycle-policy.json.tpl", {})
  repository = aws_ecr_repository.app.name
}
