################################################
# CodeDeploy
################################################
data "aws_iam_policy_document" "codedeploy" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codedeploy.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codedeploy" {
  assume_role_policy = data.aws_iam_policy_document.codedeploy.json
  name               = "${var.app}-codedeploy"
  tags = {
    Name = "${var.app}-codedeploy"
  }
}

resource "aws_iam_role_policy_attachment" "codedeploy" {
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS"
  role       = aws_iam_role.codedeploy.name
}

################################################
# CodeBuild
################################################
data "aws_iam_policy_document" "codebuild" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codebuild" {
  assume_role_policy = data.aws_iam_policy_document.codebuild.json
  name               = "${var.app}-codebuild"
  tags = {
    Name = "${var.app}-codebuild"
  }
}

resource "aws_iam_role_policy" "codebuild" {
  name   = "CodeBuildBase"
  policy = templatefile("${path.module}/templates/codebuild-policy.json.tpl",
    {
      aws_account_id      = data.aws_caller_identity.current.account_id
      codepipeline-bucket = aws_s3_bucket.codepipeline-artifacts.id
    }
  )
  role  = aws_iam_role.codebuild.id
}

################################################
# CodePipeline
################################################
data "aws_iam_policy_document" "codepipeline" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "codepipeline" {
  assume_role_policy = data.aws_iam_policy_document.codepipeline.json
  name               = "${var.app}-codepipeline"
  tags = {
    Name = "${var.app}-codepipeline"
  }
}

resource "aws_iam_role_policy" "codepipeline" {
  name   = "CodePipelineBase"
  policy = templatefile("${path.module}/templates/codepipeline-policy.json.tpl", {})
  role   = aws_iam_role.codepipeline.id
}

################################################
# Deploy Hook
################################################
data "aws_iam_policy_document" "lambda" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role" "deploy-hook" {
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  name               = "${var.app}-deploy-hook"
  tags = {
    Name = "${var.app}-deploy-hook"
  }
}

resource "aws_iam_role_policy" "deploy-hook" {
  name   = "deploy-hook"
  policy = templatefile("${path.module}/templates/deploy-hook-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      canary_name    = var.canary_name
      function-name  = "${var.app}-deploy-hook"
    }
  )
  role   = aws_iam_role.deploy-hook.id
}

################################################
# ECS Task Execution Role
################################################
data "aws_iam_policy_document" "ecs-tasks" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type = "Service"
      identifiers = [
        "ecs-tasks.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "ecs-task-exec-role" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${var.app}-ecsTaskExecutionRole"
  tags = {
    Name = "${var.app}-ecsTaskExecutionRole"
  }
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-role-policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.ecs-task-exec-role.name
}

resource "aws_iam_role_policy_attachment" "ecs-task-exec-cw-agent" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy"
  role       = aws_iam_role.ecs-task-exec-role.name
}

################################################
# ECS Task Role
################################################
resource "aws_iam_role" "ecs-tasks" {
  assume_role_policy = data.aws_iam_policy_document.ecs-tasks.json
  name               = "${var.app}-ecsTaskRole"
  tags = {
    Name = "${var.app}-ecsTaskRole"
  }
}

resource "aws_iam_role_policy" "ecs-tasks" {
  name   = "ecs-tasks"
  policy = templatefile("${path.module}/templates/ecs-tasks-policy.json.tpl",
    {
      aws_account_id           = data.aws_caller_identity.current.account_id
      ecs-exec-logs-bucket-arn = aws_s3_bucket.ecs-exec-logs.arn
      kms-alias                = aws_kms_alias.session-manager.name
    }
  )
  role   = aws_iam_role.ecs-tasks.id
}

################################################
# Canary Role
################################################
resource "aws_iam_role" "canary" {
  assume_role_policy = data.aws_iam_policy_document.lambda.json
  name               = "${var.app}-canary"
  tags = {
    Name = "${var.app}-canary"
  }
}

resource "aws_iam_role_policy" "canary" {
  name   = "canary"
  policy = templatefile("${path.module}/templates/canary-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
    }
  )
  role   = aws_iam_role.canary.id
}
