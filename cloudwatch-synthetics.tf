/*
resource "aws_synthetics_canary" "visual-monitor" {
  name                 = "cw-synth-demo"
  artifact_s3_location = "s3://${aws_s3_bucket.canary-artifacts.id}/"
  execution_role_arn   = aws_iam_role.canary.arn
  handler              = "exports.handler"
  zip_file             = "test-fixtures/lambdatest.zip"
  runtime_version      = "syn-1.0"

  schedule {
    expression = "rate(0 minute)"
  }
  vpc_config {
    subnet_ids = [module.vpc.private-a-subnet-id, module.vpc.private-b-subnet-id]
    security_group_ids = [aws_security_group.canary.id]
  }
}
*/
