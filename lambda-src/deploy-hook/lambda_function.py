import json
import boto3
import os
import time

AWS_REGION = os.getenv('AWS_REGION')
CANARY_NAME = os.getenv('CANARY_NAME')
RUN_CANARY = os.getenv('RUN_CANARY')

CODE_DEPLOY = boto3.client('codedeploy', region_name=AWS_REGION)
SYNTHETICS = boto3.client('synthetics')

def lambda_handler(event, context):
  deploymentId = event['DeploymentId']
  lifecycleEventHookExecutionId = event['LifecycleEventHookExecutionId']
  hook_exec_status = 'Succeeded'
  if RUN_CANARY != "True":
    put_lifecycle_event_hook_execution_status(deploymentId, lifecycleEventHookExecutionId, hook_exec_status)
    return
  try:
    if canary_exists():
      SYNTHETICS.start_canary(
        Name=CANARY_NAME
      )
      wait_for_canary_to_stop()
      if get_canary_result() != "PASSED":
        hook_exec_status = 'Failed'
    put_lifecycle_event_hook_execution_status(deploymentId, lifecycleEventHookExecutionId, hook_exec_status)
  except Exception as e:
    put_lifecycle_event_hook_execution_status(deploymentId, lifecycleEventHookExecutionId, 'Failed')

def canary_exists():
  try:
    response = SYNTHETICS.get_canary(
      Name=CANARY_NAME
    )
    return True
  except SYNTHETICS.exceptions.ResourceNotFoundException as e:
    return False

def wait_for_canary_to_stop():
  time.sleep(5)
  while SYNTHETICS.get_canary(Name=CANARY_NAME)['Canary']['Status']['State'] != "STOPPED":
    time.sleep(2)
  time.sleep(3)

def get_canary_result():
  return SYNTHETICS.get_canary_runs(
    Name=CANARY_NAME
  )['CanaryRuns'][0]['Status']['State']

def put_lifecycle_event_hook_execution_status(deploymentId, lifecycleEventHookExecutionId, status):
  CODE_DEPLOY.put_lifecycle_event_hook_execution_status(
    deploymentId=deploymentId,
    lifecycleEventHookExecutionId=lifecycleEventHookExecutionId,
    status=status
  )
