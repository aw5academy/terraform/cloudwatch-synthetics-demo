terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.71"
    }
  }
  required_version = ">= 1.1.4"
}

provider "aws" {
  region  = var.region
  default_tags {
    tags = {
      App = var.app
    }
  }
}

data "aws_caller_identity" "current" {}

data "aws_kms_key" "s3" {
  key_id = "alias/aws/s3"
}
