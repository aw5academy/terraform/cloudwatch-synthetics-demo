module "vpc" {
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/vpc.git"
  ssm      = "true"
  vpc-name = var.app
}
