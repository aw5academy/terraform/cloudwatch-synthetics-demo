resource "aws_codecommit_repository" "app" {
  repository_name = var.app
  tags = {
    Name = var.app
  }
}
