resource "aws_kms_key" "session-manager" {
  deletion_window_in_days = "7"
  description             = "The encryption key for the session manager service."
  enable_key_rotation     = "true"
  policy                  = templatefile("${path.module}/templates/cmk-policy.json.tpl",
    {
      aws_account_id = data.aws_caller_identity.current.account_id
      region         = var.region
    }
  )
  tags = {
    Name = "session-manager"
  }
}

resource "aws_kms_alias" "session-manager" {
  name          = "alias/session-manager"
  target_key_id = aws_kms_key.session-manager.key_id
}
